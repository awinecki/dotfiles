e!/bin/sh

brew cask install slack
brew cask install skype
brew cask install google-chrome
brew cask install evernote
brew cask install 1password
brew cask install steermouse
brew cask install karabiner-elements
brew cask install sketch
brew cask install flux
brew cask install alfred
brew cask install bartender
brew cask install iterm2
brew cask install typora
brew cask install sourcetree
brew cask install postman
brew cask install qnapi
brew cask install mpv
brew cask install vlc
brew cask install numi
# brew cask install alacritty # very fast terminal emulator (GPU focused)

# brew cask install google-drive
# brew cask install dropbox
brew cask install qnapi
brew cask install pgadmin4
brew cask install skype
brew cask install skitch
brew cask install steam
brew cask install transmission
brew cask install mpv
brew cask install spotify
brew cask install spectacle
brew cask install imageoptim
brew cask install toggl
brew cask install sequel-pro
