#!/bin/sh

# Code
brew install rbenv
brew install nvm
brew install httpie
brew install wget
brew install zsh zsh-completions
brew install tmux
brew install ripgrep
brew install mtr
brew install watch
brew install hub # https://github.com/github/hub
brew install jez/formulae/git-heatmap
brew install diff-so-fancy
#brew install hadolint # dockerfile linter
brew install hyperfine # benchmark commands
brew install exa # modern `ls` alternative
brew install mdcat
brew install sk # fzf written in Rust
brew install fzf

brew install reattach-to-user-namespace
brew install htop
brew install heroku/brew/heroku
brew install yarn
brew install bat
brew install lazygit
brew install lazydocker
brew install dive
brew install ctop
brew install bash-completion
# brew install git
brew install wp-cli # Wordpress CLI [very optional ;)]

# Brew services
brew install nginx
brew install postgresql
brew install mysql@5.7
brew install php@7.1
# Git Time Metric
brew tap git-time-metric/gtm
brew install gtm
brew install mosh
brew install ncdu
