#!/bin/sh

brew install zsh zsh-completions

# Install oh my zsh!
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
