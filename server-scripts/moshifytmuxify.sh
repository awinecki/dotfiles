#!/usr/bin/env bash

# Ubuntu install mosh & tmux
sudo apt-get update
sudo apt-get install mosh tmux

# Get tmux conf
wget -O ~/.tmux.conf https://raw.githubusercontent.com/awinecki/dotfiles/master/tmux/.tmux.conf
# Remove top desktop-only settings from .tmux.conf file
sed -i -e 1,11d ~/.tmux.conf
# Install TPM https://github.com/tmux-plugins/tpm
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
# Reload TMUX conf
tmux start-server
tmux source ~/.tmux.conf

echo "To install TPM plugins, do this:\n"
echo "Press prefix + I (capital i, as in Install) to fetch the plugin."
