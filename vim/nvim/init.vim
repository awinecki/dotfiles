" VIM user: @awinecki (Andrzej Winecki), winecki@gmail.com
" https://github.com/awinecki
"
" gl & happy vimming!
"
" ========================================================================
" ============================== GENERAL =================================
" ========================================================================

set langmenu=en_US.UTF-8
set fileencoding=utf-8
let mapleader=' '
set autoread " After opened file changes, avoid showing prompt
set backspace=indent,eol,start " allow backspacing over everything in ins mode
set noautochdir
set list
set nofixeol
" Better diffs; src; https://vimways.org/2018/the-power-of-diff/
set diffopt+=algorithm:patience,indent-heuristic,vertical
set clipboard=unnamed
set noswapfile
set backup
set backupdir=/tmp
set directory=/tmp
set writebackup
set undofile " Save undo's after file closes
set undodir=/tmp " where to save undo histories
set undolevels=1000 " How many undos
set undoreload=10000
set guicursor=i:block
set hidden " Hides buffers instead of closing them
" Indentation
set autoindent
set copyindent " Copies previous indentation
set showmatch " Jump to parentheses
set ignorecase
set smartcase
set hlsearch " highlight search terms
set history=500 " remember more commands and search history
set title " change the terminal's title

set tabstop=2
set shiftwidth=2
set expandtab
set nocompatible
set modelines=0
set showmode
" set textwidth=80 (auto breaks lines at 80)
set colorcolumn=+1

" Syntax
syntax on
syntax enable

filetype plugin indent on

if has('mouse')
    set mouse=a
endif

" " Always show statusline
set laststatus=2

" Hide separator between split views
set fillchars+=vert:\ 

" Neovim Python paths
let g:python_host_prog = '/Users/awinecki/.pyenv/versions/neovim2/bin/python'
let g:python3_host_prog = '/Users/awinecki/.pyenv/versions/neovim3/bin/python'

"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
set termguicolors

" ========================================================================
" ============================== FILETYPES RECOGNITION ===================
" ========================================================================

augroup markdown
    au!
    au BufNewFile,BufRead *.md,*.markdown setlocal filetype=markdown
augroup END

" ========================================================================
" ============================== PLUGINS =================================
" ========================================================================

call plug#begin()

" Colorschemes
Plug 'morhetz/gruvbox'
Plug 'ajh17/Spacegray.vim'
Plug 'mhartington/oceanic-next'
Plug 'liuchengxu/space-vim-dark'
Plug 'MaxSt/FlatColor'
Plug 'mhinz/vim-janah'
Plug 'iCyMind/NeoSolarized'
Plug 'joshdick/onedark.vim'
Plug 'romainl/Apprentice'
Plug 'ayu-theme/ayu-vim'
Plug 'cocopon/iceberg.vim'

" Movement
" Seamless ctrl+h/j/k/l pane switching supporting tmux
Plug 'benmills/vimux'
Plug 'christoomey/vim-tmux-navigator'
Plug 'justinmk/vim-sneak'


" Syntax
Plug 'martinda/Jenkinsfile-vim-syntax'
Plug 'StanAngeloff/php.vim'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'chr4/nginx.vim'
Plug 'slim-template/vim-slim'

Plug 'leafgarland/typescript-vim'
Plug 'peitalin/vim-jsx-typescript'
" Plug 'othree/yajs.vim'
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
" If it doesnt work, go to ~/.config/nvim/plugged/neomake/...
" ft/dockerfile.vim and comment out autoloaders that fail -.-
Plug 'ekalinin/Dockerfile.vim'
Plug 'ryanoasis/vim-devicons'

" Plug 'prettier/vim-prettier', {
"   \ 'do': 'yarn install',
"   \ 'branch': 'release/1.x',
"   \ 'for': [
"     \ 'javascript',
"     \ 'swift' ] }

    " \ 'typescript',
    " \ 'css',
    " \ 'less',
    " \ 'scss',
    " \ 'json',
    " \ 'graphql',
    " \ 'markdown',
    " \ 'vue',
    " \ 'lua',
    " \ 'php',
    " \ 'python',
    " \ 'ruby',
    " \ 'html',

" post install (yarn install | npm install) then load plugin only for editing supported files
" Plug 'prettier/vim-prettier', {
"   \ 'do': 'yarn install',
"   \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html'] }

" Plug 'sbdchd/neoformat'
Plug 'w0rp/ale'
let g:ale_fixers = {
\   'javascript': ['prettier'],
\   'css': ['prettier'],
\}

" let g:ale_sign_error = '>>'
" let g:ale_sign_warning = '--'
let g:ale_sign_warning = '㆔'
let g:ale_sign_error = '♨'
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter% / %code%] %s [%severity%]'

" Git
Plug 'tpope/vim-fugitive'
" Plug 'junegunn/gv.vim'
" Plug 'airblade/vim-gitgutter'

" Lints, etc.
" Plug 'nvie/vim-flake8'
" Plug 'maksimr/vim-jsbeautify'
" Plug 'einars/js-beautify'
" Plug 'vim-syntastic/syntastic'
" Plug 'wookiehangover/jshint.vim'

" Sessions
Plug 'tpope/vim-obsession'

" LSP
" Plug 'autozimu/LanguageClient-neovim', {
"     \ 'branch': 'next',
"     \ 'do': 'bash install.sh',
"     \ }

" Linters and formatters
" Plug 'ambv/black'

" Debuggers
" Plug 'vim-vdebug/vdebug'

" Plugins
" Plug 'chrisbra/Colorizer'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
"Plug 'neomake/neomake'
Plug 'scrooloose/nerdtree'
Plug 'mattn/emmet-vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'junegunn/goyo.vim'

function! DoRemote(arg)
    UpdateRemotePlugins
endfunction

Plug 'Shougo/deoplete.nvim', { 'do': function('DoRemote') }

call plug#end()
" call neomake#configure#automake('rw', 1000)
