" Rg
nnoremap K :Rg! "\b<C-R><C-W>\b"<CR>:cw<CR>
" NERDTree
map <C-N> :NERDTreeToggle <CR>
" Emmet
let g:user_emmet_leader_key='<C-Z>' " Emmet key, replace default Y
" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>
" Movement between splits
nmap <silent> <C-k> :wincmd k<CR>
nmap <silent> <C-j> :wincmd j<CR>
nmap <silent> <C-h> :wincmd h<CR>
nmap <silent> <C-l> :wincmd l<CR>
" Tabs
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove<cr>
" Reload, source
map <leader>lo :source %<cr>
map <leader>lr :source ~/.config/nvim/init.vim<cr>
" Toggle conceallevel
map <leader>lc :exec &conceallevel ? "set conceallevel=0" : "set conceallevel=1"<CR>
map <leader>lf :ALEFix<CR>
map <leader>lk :set expandtab<CR>:retab<CR>


" Spacemacs inspired commmands
" Searches: <SPACE>s
map <Leader>sa :Rg<CR>
map <Leader>ss :BLines<CR>
map <Leader>sl :Lines<CR>
map <Leader>] :lnext<CR>
map <Leader>[ :lprev<CR>
map <Leader>kx :Gdiffsplit!<CR>
map <Leader>ks :Gwrite!<CR>
map <Leader>kl :diffget //3<CR>
map <Leader>kh :diffget //2<CR>
map <Leader>lq :qal<CR>

map <Leader>q :q<CR>
map <Leader>Q :bdelete<CR>
map <Leader>a :FZF<CR>
" Requires FZF-vim modified version w/ Sessions: command
nmap <Leader>e :Sessions<CR>
map <Leader>t8 :set colorcolumn=+1<CR>
map <Leader>t0 :set colorcolumn=0<CR>
" Toggle line numbers
map <Leader>tl :set number!<CR>
map <Leader>tr :set relativenumber!<CR>
map <Leader>w[ :cprev<CR>
map <Leader>w] :cnext<CR>

nnoremap <Leader>sr :%s///g<left><left><left>
vnoremap <C-r> "hy:%s/<C-r>h//gc<left><left><left>

" FZF.vim
nnoremap <silent> <leader><space> :Files<CR>
nnoremap <silent> <leader>p :Buffers<CR>
nnoremap <silent> <leader>; :BLines<CR>
nnoremap <silent> <leader>. :Lines<CR>
nnoremap <silent> <leader>o :BTags<CR>
nnoremap <silent> <leader>O :Tags<CR>
nnoremap <silent> <leader>? :History<CR>
nnoremap <silent> <leader>/ :execute 'Ag ' . input('Ag/')<CR>
nnoremap <silent> K :call SearchWordWithAg()<CR>
vnoremap <silent> K :call SearchVisualSelectionWithAg()<CR>
nnoremap <silent> F :call SearchWordWithDash()<CR>
vnoremap <silent> F :call SearchVisualSelectionWithDash()<CR>
nnoremap <silent> <leader>gl :Commits<CR>
nnoremap <silent> <leader>ga :BCommits<CR>

" imap <C-x><C-f> <plug>(fzf-complete-file-ag)
" imap <C-x><C-l> <plug>(fzf-complete-line)

map <leader>oc :Colors<CR>
map <leader>od :lcd %:p:h<CR>
map <leader>os <Plug>(ToggleListchars)
" Reset current file
map <leader>oe :edit!<CR>
map <leader>oz :Goyo<cr>

" Open current file in Typora
map <leader>ot :!open -a typora "%"<cr>

" Display messages
map <Leader>om :messages<CR>

" Edit main VIM config file
map <Leader>fd :e ~/.config/nvim/init.vim<cr>

" Windows: <SPACE>w
map <Leader>wo :tabnext<cr>
map <Leader>wO :tabNext<cr>
map <Leader>0 :vsplit<cr>
map <Leader>- :split<cr>

" Movement
map <Leader>wh :wincmd h<cr>
map <Leader>wj :wincmd j<cr>
map <Leader>wk :wincmd k<cr>
map <Leader>wl :wincmd l<cr>

map <Leader>ww <C-w>w
map <Leader><tab> <C-w>w
map <Leader>= <C-w>=
map <Leader>wW <C-w>W

" Matchit mapping
map <Leader>m %

nmap <silent> ,/ :nohlsearch<CR>
map <silent> \] :cn<cr>
map <silent> \[ :cp<cr>
map <silent> \; :so %<CR>
" Use ; instead of : (less pressing Shift)
nnoremap ; :
" Get out of insert mode with 'jj'
inoremap jj <ESC>

:map <Leader>hh :diffget //2 <bar> diffup<CR>
:map <Leader>ll :diffget //3 <bar> diffup<CR>
:map <Leader>8 080l

map <C-p> :FZF<CR>

" Map control-backspace to delete the previous word
:imap <C-BS> <C-W>

imap <C-Z> <ESC>

:vnoremap ss :sort<CR>



" if &diff
"     map <leader>1 :diffget BASE<CR>
"     map <leader>2 :diffget LOCAL<CR>
"     map <leader>3 :diffget REMOTE<CR>
" endif
