" NEOMAKE

" function! MyOnBattery()
"   return readfile('/sys/class/power_supply/AC/online') == ['0']
" endfunction

" if MyOnBattery()
"   call neomake#configure#automake('w')
" else
"   call neomake#configure#automake('nw', 1000)
" endif

" let g:neomake_python_enabled_makers = ['flake8']
" let g:neomake_javascript_enabled_makers = ['standard']
" let g:neomake_typescript_enabled_makers = ['eslint']
" let g:neomake_javascript_enabled_makers = ['eslint']
let g:neomake_verbose = 1
