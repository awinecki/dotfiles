" Airline
" let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_section_b = '%{strftime("%a %-d/%-m %-I:%M%p")} ✭'
let g:airline_section_y = 'BN:%{bufnr("%")} %{ObsessionStatus()}'
