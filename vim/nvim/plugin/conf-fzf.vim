let g:fzf_layout = {'down': '60%'}

" Likewise, Files command with preview window
command! -bang -nargs=? -complete=dir Files
  \ call fzf#vim#files(<q-args>, fzf#vim#with_preview(), <bang>0)

" Similarly, we can apply it to fzf#vim#grep. To use ripgrep instead of ag:
command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color=always --smart-case '.shellescape(<q-args>), 1,
  \   <bang>0 ? fzf#vim#with_preview('up:60%')
  \           : fzf#vim#with_preview('right:50%'),
  \   <bang>0)

function! s:sessions(...)
    call fzf#run({
                \ 'source':  'ls -1 ~/.vim/session',
                \ 'sink':    'SLoad',
                \ 'options': '+m --prompt="Sessions> "',
                \ 'down':    '20%'
                \})
endfunction
command! Sessions call s:sessions()
