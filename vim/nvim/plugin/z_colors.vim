" colorscheme iceberg
let g:airline_theme="iceberg"
colors OceanicNext

" Gruvbox
" let g:gruvbox_contrast_dark = 'hard'
" let g:gruvbox_contrast_dark = 'soft'
" colors gruvbox
" set background=dark
" let g:airline_theme = 'gruvbox'
