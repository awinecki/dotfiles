rvm_current() {
  rvm current 2>/dev/null
}

rbenv_version() {
  rbenv version 2>/dev/null | awk '{print $1 "•rb"}'
}

# Virtualenv: current working virtualenv
prompt_virtualenv() {
  local virtualenv_path="$VIRTUAL_ENV"
  if [[ -n $virtualenv_path && -n $VIRTUAL_ENV_DISABLE_PROMPT ]]; then
    echo -n " %{$FG[029]%}`basename $virtualenv_path`•py %{$reset_color%}"
  fi
}

# ZSH_THEME_NVM_PROMPT_PREFIX="%B⬡%b "
# ZSH_THEME_NVM_PROMPT_SUFFIX=""

PROMPT='%{$FG[239]%}${PWD/#$HOME/~}%{$reset_color%}$(git_prompt_info) %{$FG[240]%}㆔%{$reset_color%} '

# ⭠
# ❭
# 〉
# フ ㆚ ㆔
# ₪
# λ
# ♈︎ ⎇

# Colors
# 081, 045 blue
# 178 Orange


ZSH_THEME_GIT_PROMPT_PREFIX="%{$FG[074]%} "
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY=" %{$FG[126]%}•"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[green]%}?"
ZSH_THEME_GIT_PROMPT_CLEAN=""

export VIRTUAL_ENV_DISABLE_PROMPT=no

RPROMPT='$(prompt_virtualenv) %{$FG[052]%}$(rbenv_version)%{$reset_color%}%{$FG[032]%}%(1V.(%1v).)%{$reset_color%}'
