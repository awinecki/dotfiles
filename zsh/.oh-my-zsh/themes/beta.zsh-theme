JSICON="\uE74E"
RBICON="\uE791"
GHICON="\uF7A1"
GHUBCATICON="\uE708"
GHUBCATICON2="\uE709"
BRANCHICON="\uE725"
BOLT="\uF0E7"
ZAP="\u26A1" # DONT USE THIS; BREAKS FORMATTING!!!
ANGLE_RIGHT="\uF105"
CHEVRON_DOUBLE_RIGHT="\uF63D"
DELTA="\uF6C1"

rbenv_version() {
  echo $(rbenv version 2>/dev/null | awk '{print $1 }')
}

RBENV_PREFIX=" $RBICON "
ZSH_THEME_NVM_PROMPT_PREFIX=" $JSICON "

function nvm_prompt_info() {
  if [ "$(whence -w __init_nvm)" = "__init_nvm: function" ]; then
    return
  fi
  local nvm_prompt
  nvm_prompt=$(node -v 2>/dev/null)
  [[ "${nvm_prompt}x" == "x" ]] && return
  nvm_prompt=${nvm_prompt:1}
  echo "${ZSH_THEME_NVM_PROMPT_PREFIX}${nvm_prompt}${ZSH_THEME_NVM_PROMPT_SUFFIX}"
}

function rbenv_prompt_info() {
  if [ "$(whence -w load_rbenv)" = "load_rbenv: function" ]; then
    return
  else
    echo "$RBENV_PREFIX$(rbenv_version)$RBENV_SUFFIX"
  fi
}

PROMPT='%{$FG[240]%}㆔%{$reset_color%} '
#PROMPT=""
RPS1='%{$fg[white]%}%2~%{$FG[161]%}$(rbenv_prompt_info)%{$reset_color%}%{$FG[081]%}$(nvm_prompt_info)%{$reset_color%}$(git_prompt_info)'

ZSH_THEME_GIT_PROMPT_PREFIX=" %{$fg[yellow]%}$GHUBCATICON "
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN=""
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%} $DELTA%{$reset_color%}"
