# HEROKU_AC_ZSH_SETUP_PATH=/Users/awinecki/Library/Caches/heroku/autocomplete/zsh_setup && test -f $HEROKU_AC_ZSH_SETUP_PATH && source $HEROKU_AC_ZSH_SETUP_PATH;

# Composer
export PATH=$PATH:/Users/awinecki/.composer/vendor/bin
export PATH=$PATH:/usr/lib/lightdm/lightdm:/usr/local/sbin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/local/bin

# Node modules
export PATH=$HOME/.local/bin:/usr/local/lib/node_modules:$PATH
export PATH="/Applications/Postgres93.app/Contents/MacOS/bin:$PATH"
export PATH=/usr/local/bin:$PATH
export PATH=/usr/local/npm-global/bin:$PATH
export PATH=/usr/local/git/bin:$PATH
export PATH="/usr/local/opt/mysql@5.7/bin:$PATH"

export PYTHONPATH=$PYTHONPATH:/usr/lib/python2.7/site-packages
export PKG_CONFIG_PATH="/usr/local/opt/readline/lib/pkgconfig" # asciinema

