port() {
  lsof -wni tcp:$1
}

yml2nix() {
  echo "$(cat $1 | sed s/:\ /=/ | sed /^$/d | sed '/^#/ d')"
}
