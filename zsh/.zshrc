# PROFILE_STARTUP=false
# [[ "$PROFILE_STARTUP" == true ]] && {
#   PS4=$'%D{%M%S%.} %N:%i> '
#   exec 3>&2 2>$HOME/tmp/startlog.$$
#   setopt xtrace prompt_subst
# }
# ---------------------------------------------------------

# ZSH / OH-MY-ZSH config
ZSH=$HOME/.oh-my-zsh
ZSH_CONF="$HOME/.dotfiles/zsh"
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
plugins=(git docker docker-compose mosh ripgrep)
# plugins=(git)
# ZSH_THEME="alpha"
ZSH_THEME="beta"
# ZSH_THEME="random"
# ZSH_THEME="terminalparty"
export TERM="xterm-256color"
COMPLETION_WAITING_DOTS="true"
source $ZSH/oh-my-zsh.sh
setopt extendedglob
setopt NO_NOMATCH

# Load aliases
[ -f "$ZSH_CONF/.aliases.sh" ] && source "$ZSH_CONF/.aliases.sh" || echo "No .aliases.sh file found!"

# Load aliases
[ -f "$ZSH_CONF/.functions.sh" ] && source "$ZSH_CONF/.functions.sh" || echo "No .functions.sh file found!"

# Source all env vars
set -o allexport
. $ZSH_CONF/.env
. $ZSH_CONF/.secrets
set +o allexport

# Load PATH updates
. $ZSH_CONF/.path.sh
# Load scripts
. $ZSH_CONF/../scripts/z.sh
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Lazy pyenv loading
alias load_pyenv='eval "$(pyenv init -)"'
alias pyenv='unalias pyenv && load_pyenv && pyenv'
# PYENV_ROOT="$HOME/.pyenv"

# Lazy rbenv loading
function load_rbenv {
  unset -f rbenv
  unset -f load_rbenv
  eval "$(rbenv init -)"
}
rbenv() {
  load_rbenv
  rbenv "$@"
}

# Defer initialization of nvm until nvm, node or a
# node-dependent command is run. Ensure this block is only
# run once if .bashrc gets sourced multiple times
# by checking whether __init_nvm is a function.
# also, check https://github.com/nvm-sh/nvm/issues/1277#issuecomment-536218082
if [ -s "$HOME/.nvm/nvm.sh" ] && [ ! "$(whence -w __init_nvm)" = function ]; then
  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/bash_completion" ] && . "$NVM_DIR/bash_completion"
  declare -a __node_commands=('nvm' 'node' 'npm' 'yarn' 'gulp' 'grunt' 'webpack')
  function __init_nvm() {
    for i in "${__node_commands[@]}"; do unalias $i; done
    . "$NVM_DIR"/nvm.sh
    unset __node_commands
    unset -f __init_nvm
  }
  for i in "${__node_commands[@]}"; do alias $i='__init_nvm && '$i; done
fi

# Google Cloud SDK
# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/awinecki/Downloads/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/awinecki/Downloads/google-cloud-sdk/path.zsh.inc'; fi
# The next line enables shell command completion for gcloud.
if [ -f '/Users/awinecki/Downloads/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/awinecki/Downloads/google-cloud-sdk/completion.zsh.inc'; fi

# ------------------------------------------------
# [[ "$PROFILE_STARTUP" == true ]] && { # Profile end
#   unsetopt xtrace
#   exec 2>&3 3>&-
# }
